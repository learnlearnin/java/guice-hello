package in.learnlearn.java.di;

import com.google.inject.AbstractModule;

public class PrinterModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Printer.class).to(PaperPrinter.class);
    }
}
