package in.learnlearn.java.di;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Injector injector = Guice.createInjector(new PrinterModule());
        PrinterService printerService = injector.getInstance(PrinterService.class);

        printerService.getPrinted("hello world");
    }
}
