package in.learnlearn.java.di;

import javax.inject.Inject;

public class PrinterService {
    private final Printer printer;

    @Inject
    public PrinterService(Printer printer) {
        this.printer = printer;
    }

    public void getPrinted(String input) {
        printer.print(input);
    }

}
