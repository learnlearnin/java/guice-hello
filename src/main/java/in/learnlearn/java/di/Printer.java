package in.learnlearn.java.di;

public interface Printer {
    void print(String input);
}